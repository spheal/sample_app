import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class PythonOrgSearch(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://localhost:32768")
    def login(self,user,passwd):
        self.driver.find_element_by_link_text("Login").click()
        username = self.driver.find_element_by_id("username")
        password = self.driver.find_element_by_id("password")
        username.send_keys(user)
        password.send_keys(passwd)
        self.driver.find_element_by_id("submit").click()
        assert "Note Title" in self.driver.page_source
        assert "Note Saved on" in self.driver.page_source
    # def test_new_tag(self):
    #     self.driver.find_element_by_link_text("NEW TAG").click()
    #     new_tag = self.driver.find_element_by_id("tag")
    #     new_tag.send_keys("Selenium_Test")
    #     self.driver.find_element_by_id("submit").click()

    def test_new_note(self):
        self.login("test","123")
        title_name = "Selenium_Test"
        self.driver.find_element_by_link_text("NEW NOTE").click()
        title = self.driver.find_element_by_id("note_title")
        note = self.driver.find_element_by_id("flask-pagedown-note")
        title.send_keys = title_name
        note.send_keys("This is simple note that was added during selenium tests.")
        self.driver.find_element_by_id("submit").click()
        self.driver.find_element_by_link_text("VIEW ALL NOTES").click()
        assert title_name in self.driver.page_source
    def test_signup(self):
        new_user = { "user": "Bred","email": "bred@scr.ua","password": "qwerty" }
        self.driver.find_element_by_link_text("Sign Up").click()
        username = self.driver.find_element_by_id("username")
        email = self.driver.find_element_by_id("email")
        password = self.driver.find_element_by_id("password")
        confirm_password = self.driver.find_element_by_id("confirm_password")
        username.send_keys(new_user["user"])
        email.send_keys(new_user["email"])
        password.send_keys(new_user["password"])
        confirm_password.send_keys(new_user["password"])
        self.driver.find_element_by_id("submit").click()
        assert "Welcome," + new_user["user"] in self.driver.page_source
    def setDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
