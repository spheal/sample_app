# A-Simple-Note-Taking-Web-App
An easy to use and deploy web app built using Flask

# Features:

* Simple Web application, easy to use and *very* easy to deploy locally
* Written in simple Python. Even a beginner Python developer can contribute to this
* Support for SQLite, so you can easily play with it
* REST API for retrieving data easily

# Requirements:

Execute the following command to install the required third party libraries:<br />

`pip3 install -r requirements.txt`

# Usage:
Install the dependencies by simply executing:

`pip3 install -r requirements.txt`

Run this command to start the app:

`python3 manage.py`

Visit `0.0.0.0:5000` on your web browser


